# CDS Packaging

This repo holds documentation and issues for the Advanced LIGO CDS
group's packaging of software for instrument control and analysis.

* [CDSSoft wiki](https://git.ligo.org/cds-packaging/docs/wikis/home)

* [CDSSoft issue tracker](https://git.ligo.org/cds-packaging/docs/issues)

The wiki holds information on how to enable the CDSSoft software
archives.
